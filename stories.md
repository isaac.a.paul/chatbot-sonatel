## story goodbye
* Goodbye
    - utter_goodbye
    - action_restart



## story salut
* Salutations
    - utter_ask_name
* Name{"Nom_client" : "Isaac Paul"}
    - slot{"Nom_client" : "Isaac Paul"}
    - utter_salut

## story salut1
* Salutations
    - utter_ask_name
* Name{"Nom_client" : "Cheirk"}
    - slot{"Nom_client" : "Cheirk"}
    - utter_salut
* Goodbye
    - utter_goodbye
    - action_restart

## OM
* Name{"Nom_client" : "Cedric"}
    - slot{"Nom_client" : "Cedric"}
    - utter_salut
* Orange_money
    - utter_ask_phone_number
* Phone_no{"phone_no" : "777776666"}
    - slot{"phone_no" : "777776666"}
    - utter_om
    - utter_goodbye
* Goodbye
    - utter_goodbye
    - action_restart


## story OM1
* Salutations
    - utter_ask_name
* Name{"Nom_client" : "Hadija"}
    - slot{"Nom_client" : "Hadija"}
    - utter_salut
* Orange_money
    - utter_ask_phone_number
* Phone_no{"phone_no" : "777776666"}
    - slot{"phone_no" : "777776666"}
    - utter_om
    - utter_goodbye
* Goodbye
    - utter_goodbye
    - action_restart

## story reclamation
* Reclamation
    - utter_ask_phone_number
* Phone_no{"phone_no" : "777776665"}
    - slot{"phone_no" : "777776665"}
    - utter_reclamation
    - utter_goodbye
    - action_restart

## story reclamation1
* Reclamation
    - utter_ask_phone_number
* Phone_no{"phone_no" : "773846659"}
    - slot{"phone_no" : "773846659"}
    - utter_reclamation
    - utter_goodbye
* Goodbye
    - utter_goodbye
    - action_restart
    
    
## Generated Story 1701770681134987716
* Salutations
    - utter_ask_name
* Name{"Nom_client" : "Nafisatou"}
    - slot{"Nom_client" : "Nafisatou"}
    - utter_salut
* Reclamation
    - utter_ask_phone_number
* Phone_no{"phone_no" : "773846723"}
    - slot{"phone_no" : "773846723"}
    - utter_reclamation
    - utter_goodbye
* Goodbye
    -utter_goodbye
    - action_restart
    
## android story
* Salutations
    - utter_ask_name
* Name{"Nom_client" : "Isaac"}
    - slot{"Nom_client" : "Isaac"}
    - utter_salut
* Internet_config
    - utter_ask_phone_OS
* Internet_config_android{"android": "Tecno"}
    - slot{"phone_OS": "android"}
    - utter_confirm_android
* Yes_affirm
    - utter_settings_android
* Browsing_success
    - utter_success_config
* Goodbye
    - utter_goodbye
    - action_restart


## android story1
* Internet_config
    - utter_ask_phone_OS
* Internet_config_android
    - utter_confirm_android
* Yes_affirm
    - utter_settings_android
* Browsing_success
    - utter_success_config
* Goodbye
    - utter_goodbye
    - action_restart


## android story2
* Internet_config_android
    - utter_confirm_android
* Yes_affirm
    - utter_settings_android
* Browsing_success
    - utter_success_config
* Goodbye
    - utter_goodbye
    - action_restart


## android story not browsing
* Salutations
    - utter_ask_name
* Name{"Nom_client" : "Sandene"}
    - slot{"Nom_client" : "Sandene"}
    - utter_salut
* Internet_config
    - utter_ask_phone_OS
* Internet_config_android
    - utter_confirm_android
* Yes_affirm
    - utter_settings_android
* Not_connected
    - utter_not_connected
* Goodbye
    - utter_goodbye
    - action_restart


## android story not browsing1
* Internet_config
    - utter_ask_phone_OS
* Internet_config_android
    - utter_confirm_android
* Yes_affirm
    - utter_settings_android
* Not_connected
    - utter_not_connected
* Goodbye
    - utter_goodbye
    - action_restart


## android story not browsing2
* Internet_config_android
    - utter_confirm_android
* Yes_affirm
    - utter_settings_android
* Not_connected
    - utter_not_connected
* Goodbye
    - utter_goodbye
    - action_restart

## not android but blackberry
* Salutations
    - utter_ask_name
* Name{"Nom_client" : "Abdul"}
    - slot{"Nom_client" : "Abdul"}
    - utter_salut
* Internet_config
    - utter_ask_phone_OS
* Internet_config_android
    - utter_confirm_android
* Not_OS
    - utter_not_OS
* Internet_config_blackberry
    - utter_confirm_blackberry
* Yes_affirm
    - utter_settings_blackberry
* Browsing_success
    - utter_success_config
* Goodbye
    - utter_goodbye
    - action_restart


## not android but windows
* Salutations
    - utter_ask_name
* Name{"Nom_client" : "Samba"}
    - slot{"Nom_client" : "Samba"}
    - utter_salut
* Internet_config
    - utter_ask_phone_OS
* Internet_config_android
    - utter_confirm_android
* Not_OS
    - utter_not_OS
* Internet_config_windows
    - utter_confirm_windows
* Yes_affirm
    - utter_settings_windows
* Browsing_success
    - utter_success_config
* Goodbye
    - utter_goodbye
    - action_restart


## not android but blackberry1
* Internet_config
    - utter_ask_phone_OS
* Internet_config_android
    - utter_confirm_android
* Not_OS
    - utter_not_OS
* Internet_config_blackberry
    - utter_confirm_blackberry
* Yes_affirm
    - utter_settings_blackberry
* Browsing_success
    - utter_success_config
* Goodbye
    - utter_goodbye
    - action_restart


## not windows but android
* Salutations
    - utter_ask_name
* Name{"Nom_client" : "Ndao"}
    - slot{"Nom_client" : "Ndao"}
    - utter_salut
* Internet_config
    - utter_ask_phone_OS
* Internet_config_windows
    - utter_confirm_windows
* Not_OS
    - utter_not_OS
* Internet_config_android
    - utter_confirm_android
* Yes_affirm
    - utter_settings_android
* Browsing_success
    - utter_success_config
* Goodbye
    - utter_goodbye
    - action_restart


## not windows but blackberry
* Salutations
    - utter_ask_name
* Name{"Nom_client" : "Aissatou"}
    - slot{"Nom_client" : "Aissatou"}
    - utter_salut
* Internet_config
    - utter_ask_phone_OS
* Internet_config_windows
    - utter_confirm_windows
* Not_OS
    - utter_not_OS
* Internet_config_blackberry
    - utter_confirm_blackberry
* Yes_affirm
    - utter_settings_blackberry
* Browsing_success
    - utter_success_config
* Goodbye
    - utter_goodbye
    - action_restart
   

## not blackberry but android
* Salutations
    - utter_ask_name
* Name{"Nom_client" : "Ibrahima"}
    - slot{"Nom_client" : "Ibrahima"}
    - utter_salut
* Internet_config
    - utter_ask_phone_OS
* Internet_config_blackberry
    - utter_confirm_blackberry
* Not_OS
    - utter_not_OS
* Internet_config_android
    - utter_confirm_android
* Yes_affirm
    - utter_settings_android
* Browsing_success
    - utter_success_config
* Goodbye
    - utter_goodbye
    - action_restart


## not blackberry but windows
* Salutations
    - utter_ask_name
* Name{"Nom_client" : "Abdul-Karim"}
    - slot{"Nom_client" : "Abdul-Karim"}
    - utter_salut
* Internet_config
    - utter_ask_phone_OS
* Internet_config_blackberry
    - utter_confirm_blackberry
* Not_OS
    - utter_not_OS
* Internet_config_windows
    - utter_confirm_windows
* Yes_affirm
    - utter_settings_windows
* Browsing_success
    - utter_success_config
* Goodbye
    - utter_goodbye
    - action_restart



## windows story
* Salutations
    - utter_ask_name
* Name{"Nom_client" : "Khadidjatou"}
    - slot{"Nom_client" : "Khadidjatou"}
    - utter_salut
* Internet_config
    - utter_ask_phone_OS
* Internet_config_windows
    - utter_confirm_windows
* Yes_affirm
    - utter_settings_windows
* Browsing_success
    - utter_success_config
* Goodbye
    - utter_goodbye
    - action_restart
    

## windows story1
* Internet_config
    - utter_ask_phone_OS
* Internet_config_windows
    - utter_confirm_windows
* Yes_affirm
    - utter_settings_windows
* Browsing_success
    - utter_success_config
* Goodbye
    - utter_goodbye
    - action_restart


## windows story2
* Internet_config_windows
    - utter_confirm_windows
* Yes_affirm
    - utter_settings_windows
* Browsing_success
    - utter_success_config
* Goodbye
    - utter_goodbye
    - action_restart


## windows story not browsing
* Salutations
    - utter_ask_name
* Name{"Nom_client" : "Bright"}
    - slot{"Nom_client" : "Bright"}
    - utter_salut
* Internet_config
    - utter_ask_phone_OS
* Internet_config_windows
    - utter_confirm_windows
* Yes_affirm
    - utter_settings_windows
* Not_connected
    - utter_not_connected
* Goodbye
    - utter_goodbye
    - action_restart


## windows story not browsing1
* Internet_config
    - utter_ask_phone_OS
* Internet_config_windows
    - utter_confirm_windows
* Yes_affirm
    - utter_settings_windows
* Not_connected
    - utter_not_connected
* Goodbye
    - utter_goodbye
    - action_restart

    
## blackberry story
* Salutations
    - utter_ask_name
* Name{"Nom_client" : "Cheirk Ndao"}
    - slot{"Nom_client" : "Cheirk Ndao"}
    - utter_salut
* Internet_config
    - utter_ask_phone_OS
* Internet_config_blackberry
    - utter_confirm_blackberry
* Yes_affirm
    - utter_settings_blackberry
* Browsing_success
    - utter_success_config
* Goodbye
    - utter_goodbye
    - action_restart
    
    
## blackberry story1
* Internet_config
    - utter_ask_phone_OS
* Internet_config_blackberry
    - utter_confirm_blackberry
* Yes_affirm
    - utter_settings_blackberry
* Browsing_success
    - utter_success_config
* Goodbye
    - utter_goodbye
    - action_restart
    
    
## blackberry story2
* Internet_config_blackberry
    - utter_confirm_blackberry
* Yes_affirm
    - utter_settings_blackberry
* Browsing_success
    - utter_success_config
* Goodbye
    - utter_goodbye
    - action_restart

## blackberry story not browsing
* Salutations
    - utter_ask_name
* Name{"Nom_client" : "Bathie"}
    - slot{"Nom_client" : "Bathie"}
    - utter_salut
* Internet_config
    - utter_ask_phone_OS
* Internet_config_blackberry
    - utter_confirm_blackberry
* Yes_affirm
    - utter_settings_blackberry
* Not_connected
    - utter_not_connected
* Goodbye
    - utter_goodbye
    - action_restart
    
## blackberry story not browsing1
* Internet_config
    - utter_ask_phone_OS
* Internet_config_blackberry
    - utter_confirm_blackberry
* Yes_affirm
    - utter_settings_blackberry
* Not_connected
    - utter_not_connected
* Goodbye
    - utter_goodbye
    - action_restart
    
    
## blackberry story not browsing2
* Internet_config_blackberry
    - utter_confirm_blackberry
* Yes_affirm
    - utter_settings_blackberry
* Not_connected
    - utter_not_connected
* Goodbye
    - utter_goodbye
    - action_restart