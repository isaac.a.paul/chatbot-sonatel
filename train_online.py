from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import logging
from rasa_core.policies import KerasPolicy, FallbackPolicy, MemoizationPolicy
from rasa_core.agent import Agent
from rasa_core.interpreter import RegexInterpreter
from rasa_core.train import online
from rasa_core.utils import EndpointConfig
from rasa_core.interpreter import RasaNLUInterpreter

logger = logging.getLogger(__name__)


def run_online_training(interpreter,
                          domain_file="domain.yml",
                          training_data_file='stories.md'):
    action_endpoint = EndpointConfig(url="http://localhost:5055/webhook")
    fallback = FallbackPolicy(fallback_action_name='utter_unclear', core_threshold=0.2, nlu_threshold=0.6)
    agent = Agent(domain_file,
                  policies=[MemoizationPolicy(max_history=2), KerasPolicy()],
                  interpreter=interpreter, action_endpoint=action_endpoint)
    data_file = agent.load_data(training_data_file)
    agent.train(data_file,
                       batch_size=50,
                       epochs=200,
                       max_training_samples=300)
    online.run_online_learning(agent)
    return agent

if __name__ == '__main__':
    logging.basicConfig(level="INFO")
    nlu_interpreter = RasaNLUInterpreter('./models/nlu/default/vocalbot_nlu')
    run_online_training(nlu_interpreter)