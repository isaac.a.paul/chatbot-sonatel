from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import logging

from rasa_core.agent import Agent
from rasa_core.run import serve_application
from rasa_core.utils import EndpointConfig
from rasa_core.interpreter import RegexInterpreter
from rasa_core.policies import KerasPolicy, MemoizationPolicy, FallbackPolicy
from rasa_core.interpreter import RasaNLUInterpreter
#from rasa_core.channels.socketio import SocketIOInput
from rasa_core.channels.channel import InputChannel
logger = logging.getLogger(__name__)


def train_dialogue(domain_file='domain.yml',
                   model_path='./models/dialogue',
                   training_data_file='stories.md'):
    fallback = FallbackPolicy(fallback_action_name='utter_unclear', core_threshold=0.2, nlu_threshold=0.6)
    agent = Agent(domain_file, policies=[MemoizationPolicy(max_history=3), KerasPolicy(), fallback])
    data_file = agent.load_data(training_data_file)

    agent.train(
        data_file,
        epochs=300,
        batch_size=50,
        validation_split=0.2)

    agent.persist(model_path)
    return agent


def run_sonatel_bot(serve_forever=True):
    interpreter = RasaNLUInterpreter('./models/nlu/default/vocalbot_nlu')
    action_endpoint = EndpointConfig(url="http://localhost:5055/webhook")
    agent = Agent.load('./models/dialogue', interpreter=interpreter, action_endpoint=action_endpoint)
    serve_application(agent, channel= 'cmdline' )


    return agent


if __name__ == '__main__':
    #train_dialogue()
    run_sonatel_bot()