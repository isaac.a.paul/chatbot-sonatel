from django.urls import path
from . import views
urlpatterns = [
    path('', views.chatbothome, name= "Home"),
    path('contact/', views.contact, name= "Contacts"),
    path('chat/', views.get_bot_response, name="chats"),
    path('^/post/$', views.get_bot_response, name="post")
]