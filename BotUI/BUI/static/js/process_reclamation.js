//

function onSubmit() {
        var donnees = {
            nom: $("#nom").val(),
            prenom: $("#prenom").val(),
            derangement: $("#typeDerangement").val(),
            commentaire: $("#commentaire").val(),
            numero: $("#telephone").val()

        };

        if(donnees.nom.length != 0 && donnees.prenom.length != 0 && donnees.derangement.length != 0 && donnees.commentaire.length != 0 && donnees.numero.length != 0){
            $.ajax({

                type : 'POST',
                url : "/reclam",
                data : donnees

            }).done(function (data) {
                if(data.error) {

                    launchAlert("erreur");

                }else{
                    launchAlert("success");
                }
            });
            disable_Form();

        }else {
            launchAlert("erreur");
        }

}

function launchAlert(etat) {
    if(etat == "success"){

        $("#internalBody").prepend(" <div class='alert alert-success' id='notifs'> <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a> <h4 class='alert-heading'>Succés !!!</h4><strong>Opération bien effectuée</strong></div> ");
    }else {
        $("#internalBody").prepend(" <div class='alert alert-danger' id='notifs'> <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a> <h4 class='alert-heading'>Echec !!!</h4> <strong>Erreur lors de l'opération</strong></div> ");

    }
    $("#notifs").fadeTo(5000, 500).slideUp(500, function(){
            $("#notifs").slideUp(500);
    });
}

function disable_Form() {

    $("#prenom").attr({
        disabled: true
    });

    $("#nom").attr({
        disabled: true
    });

    $("#telephone").attr({
        disabled: true
    });

    $("#commentaire").attr({
        disabled: true
    });

    $("#typeDerangement").attr({
        disabled: true
    });

    $("#submitReclam").hide();

    $("#resetReclam").hide();

}

