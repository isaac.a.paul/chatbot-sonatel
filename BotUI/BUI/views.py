from django.shortcuts import render
from flask import Blueprint
from django.http import HttpResponse

home = Blueprint('home', __name__)
def chatbothome(request):


    ''' return HttpResponse("<h1>Home page for the chatbot</h1>")'''
    '''return render(request, 'botUI/home.html')'''
    return render(request, 'home/botAccueil.html')


@home.route("/get", methods=['GET', 'POST'])
def get_bot_response(request):
    # Recupération du message de l'user
    usertxt = request.args.get('msg')

    # Recuperation de l'id du bot auquel on est connecté
    bot = request.args.get('bot')

    isformOM = False
    isformRE = False
    isformCONFIG = False

    if bot == "botHOME":
        idbotG, reponse = bot_home(usertxt)
    elif bot == "botOM":
        idbotG, isformOM, reponse = bot_om(usertxt)
    elif bot == "botCONFIG":
        idbotG, isformCONFIG, reponse = bot_config(usertxt)
    else:
        idbotG, isformRE, reponse = bot_re(usertxt)


    if isformRE:
        return render_template(reponse)
    elif isformOM:
        return reponse
    elif isformCONFIG:
        return reponse
    else:
        return jsonify({'reponse': reponse, 'bot': idbotG})



def contact(request):
    return render(request, "botUI/basic.html", {"content1": ["To contact me you could write an email to me on ", "isaac.a.paul@aims-senegal.org"]})





# Create your views here.
