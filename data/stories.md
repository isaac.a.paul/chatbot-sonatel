## story 01
* Salutations:
    - utter_salut
## story 02
* Goodbye:
    - utter_goodbye

## story 03
* Orange money:
    - utter_ask_phone_number
## story 04
* Internet config:
    - utter_ask_phone_OS

## story 05
* Reclamation:
    - utter_ask_phone_number
    
## Generated Story -2704985419875666221
* Salutations
    - utter_salut
* Orange money{"OM": "orange money"}
    - utter_ask_phone_number
* Orange money{"phone_number": "77 714 45 28"}
    - slot{"phone_number": "77 714 45 28"}
    - utter_goodbye
    - export
    
## Generated Story -889876206987591711
* Salutations
    - utter_salut
* Internet config
    - utter_ask_phone_OS
* Goodbye
    - utter_goodbye
    - export
    
## Generated Story 1701770681134987716
* Salutations
    - utter_salut
* Reclamation
    - utter_ask_phone_number
* Goodbye
    -utter_goodbye
    - export





